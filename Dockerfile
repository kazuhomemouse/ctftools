FROM ubuntu:21.04

ENV HOSTNAME ctf
ENV DEBIAN_FRONTEND=noninteractive

#
# apt
#
RUN apt update -y
RUN apt upgrade -y --fix-missing
RUN apt install -y tzdata

#
# locale
#
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

#
# packages
#
RUN apt install -y bat curl file git httpie make netcat tree unzip vim wget zsh

# bat
mkdir -p ~/.local/bin
ln -s /usr/bin/batcat ~/.local/bin/bat

#
# dotfiles
#
RUN git clone https://gitlab.com/noiv/dotfiles ~/dotfiles
RUN ln -nfs ~/dotfiles/.vimrc ~/.vimrc

#
# python
#
RUN apt install -y python3 python3-pip

#
# Crypto
#
RUN pip3 install PyCryptodome

#
# PWN
#
RUN pip3 install pwntools

#
# Reversing
#
RUN apt install -y gdb upx
RUN pip3 install angr

RUN git clone https://github.com/radareorg/radare2.git /root/radare2
RUN cd /root/radare2; sys/install.sh

#
# CMD
#
CMD ["/bin/zsh"]

