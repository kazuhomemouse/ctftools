if [ -e $1 ]; then
  # apt install exiftool
  echo "### exiftool ###"
  eval "exiftool $1"
  echo "--- END ---" 
  read Wait

  # built-in
  echo "### strings ###"
  eval "strings $1"
  echo "--- END ---" 
  read Wait

  # apt install binwalk
  echo "### binwalk ###"
  eval "binwalk -e $1"
  echo "--- END ---" 
  read Wait

else
  echo "file is not found."
fi
