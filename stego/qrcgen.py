import cv2
import numpy
import sys

def text2qr(text):
    ylen = len(text)
    xlen = len(text[0])
    img  = numpy.zeros((ylen, xlen, 3), numpy.uint8)

    for y in range(ylen):
        for x in range(xlen):
            img[y][x] = int(text[y][x]) * 255 # text[y][x] -> '0' or '1'
    return img

if __name__ == '__main__':
    filepath = (sys.argv)[1]
    with open(filepath, 'r') as f:
        text  = f.read()
        cube = text.split(', ')

        img = text2qr(cube) 
        print(img)
        cv2.imwrite('qr.png', numpy.array(img))
