#!/bin/zsh

set -eu

function show_help () {
  echo "FLAGS"
  echo "    --help   -h    show help"
  echo ""
  echo "SUBCOMMANDS"
  echo "    build          build image"
  echo ""
  echo "    run            start container"

  exit 1
}

#
# flags
#
function parse_flags () {
  # todo: use getopts
  case $1 in
    -h|--help)
      show_help
      ;;
    *)
      echo "parse error (try: vpm --help)"
      exit 1
      ;;
  esac
}

#
# parse args
#
function parse_args () {
  case $1 in
    "build")     sc_build ${@:2};;
    "run")       sc_run;;

    *)           parse_flags $@;;
  esac
}

#
# subcommands
#
function sc_build () {
  if [[ ${1:-UNSET} = UNSET ]]; then
    eval 'docker build -t ctf .'
  else
    case $1 in
      -h|--help)
        echo ""
        echo "USAGE"
        echo "    whale build [FLAG]"
        echo ""
        echo "FLAGS"
        echo "    --no-cache  -f  do not use cache when building the image"
        echo ""
        echo "    --prune     -p  execute 'docker system prune' after build"
        ;;
      -f|--no-cache)
        eval 'docker build --no-cache -t ctf .';;
      -p|--prune)
        eval 'docker build -t ctf . && docker system prune';;
      *)
        eval 'docker build -t ctf .';;
    esac
  fi
}

function sc_run () {
  eval 'docker run --name ctf --rm -it -v ~/Downloads:/ctf-files ctf /bin/zsh'
}

#
# main
#
function main () {
  if [[ ${1:-UNSET} = UNSET ]]; then
    echo "try: whale --help"
  else
    parse_args $@
  fi
}

# ------------------------- #
main $@

