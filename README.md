# CTF Tools

## Docker

```
docker build --no-cache -t ctf .
docker run --name ctf --rm -it -v ~/Downloads:/ctf-files ctf /bin/zsh
```

## Tools

* Crypto
  - [dCode.fr](https://www.dcode.fr/)
  - [PkCrack](https://www.unix-ag.uni-kl.de/~conrad/krypto/pkcrack.html)
    ```shell
    wget http://www.unix-ag.uni-kl.de/~conrad/krypto/pkcrack/pkcrack-1.2.2.tar.gz
    tar xzvf pkcrack-1.2.2.tar.gz
    cd pkcrack-1.2.2/src
    make
    ```
* Forensics
  - [usbrip](https://github.com/snovvcrash/usbrip)
  - [VOLATILITY FOUNDATION](https://www.volatilityfoundation.org/)
    - [volatility - GitHub](https://github.com/volatilityfoundation/volatility)
    - [volatility3 - GitHub](https://github.com/volatilityfoundation/volatility3)
* Reversing
  - [angr](https://github.com/angr/angr)
    - [docs](https://docs.angr.io/)
  - [GDB](https://www.gnu.org/software/gdb/)
  - [pwndbg](https://github.com/pwndbg/pwndbg)
  - radare2
    - install method: [github](https://github.com/radareorg/radare2.git)
* Stego
  - binwalk
    - [Quick Start Guide](https://github.com/ReFirmLabs/binwalk/wiki/Quick-Start-Guide)
    - repository: [GitHub](https://github.com/ReFirmLabs/binwalk)
  - exiftool
    - install method: apt
  - [Google Image Search](https://www.google.com/imghp)
  - strings
    - built-in
* Web
  - [Burp Suite](https://portswigger.net/burp)
  - [PayloadsAllTheThings](https://github.com/swisskyrepo/PayloadsAllTheThings/tree/master/SQL%20Injection)
* Mobile
  - [apktool](https://ibotpeaches.github.io/Apktool/)
    - [Install Instructions](https://ibotpeaches.github.io/Apktool/install/)
  - [jadx](https://github.com/skylot/jadx)

