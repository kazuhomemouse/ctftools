from binascii import *

for w in range(2000):
  for h in range(2000):
    ihdr = bytes([
      0x49, 0x48, 0x44, 0x52, 0x00, 0x00, w//256, w%256,
      0x00, 0x00, h//256, h%256, 0x08, 0x06, 0x00, 0x00,
      0x00])
    if crc32(ihdr)==0xb55951a1:
      print(f'width: {w}, height: {h}')
      break
