import string
import sys

def caesar(textData, key):
    result = []
    for _char in textData:
        if _char.isupper():
            if (ord(_char) + key) <= ord('Z'):
                result.append(chr(ord(_char) + key))
            else:
                result.append(chr((ord(_char) + key - ord('Z')) + ord('A') - 1))
        elif _char.islower(): 
            if (ord(_char) + key) <= ord('z'):
                result.append(chr(ord(_char) + key))
            else:
                result.append(chr((ord(_char) + key - ord('z')) + ord('a') - 1))
        else:
            result.append(_char)

    print(F'\n/* Key: {str(key)} */')
    print(''.join(result))

if __name__ == '__main__':
    filepath = (sys.argv)[1]
    with open(filepath, 'r') as f:
        textData = f.read()
        for i in range(1, 26):
            caesar(textData, i)
