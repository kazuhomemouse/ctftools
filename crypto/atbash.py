import string
import sys

def main(_str):
    plain  = list(string.ascii_letters)
    cipher = list(reversed(plain))

    for char in _str:
        if char in plain:
            print(cipher[plain.index(char)], end='')
        else:
            print(char, end='')

if __name__ == '__main__':
    main(sys.argv[1])
