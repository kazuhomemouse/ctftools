import base64
import sys

if __name__ == '__main__':
    filepath = (sys.argv)[1]
    with open(filepath, 'r') as f:
        string = f.read()
        count  = 1

        while True:
            string = base64.b64decode(string).decode()
            print(f'--- {count} ---------')
            print(string)

            flag = input('Continue: ^0 / Exit: 0\n> ')
            if flag == '0':
                exit(0)
            else:
                count += 1
