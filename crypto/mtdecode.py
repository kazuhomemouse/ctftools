import sys

SEPARATOR = '^'
CODE_MAP  = {
    '2': 'A', '22': 'B', '222': 'C',
    '3': 'D', '33': 'E', '333': 'F',
    '4': 'G', '44': 'H', '444': 'I',
    '5': 'J', '55': 'K', '555': 'L',
    '6': 'M', '66': 'N', '666': 'O',
    '7': 'P', '77': 'Q', '777': 'R', '7777': 'S',
    '8': 'T', '88': 'U', '888': 'V',
    '9': 'W', '99': 'X', '999': 'Y', '9999': 'Z',
}

def main(filename):
    with open(filename) as code:
        text = "" 
        for char in mtsplit(code.read()).split(SEPARATOR):
            text += CODE_MAP.get(char, char)
        print(text)

def mtsplit(_str):
    _list = []
    _char = None

    for char in _str:
        if char != _char and _char != None:
            _list.extend([SEPARATOR, char])
        else:
            _list.extend(char) 
        _char = char
    return "".join(_list)

if __name__ == '__main__':
    main(sys.argv[1])
