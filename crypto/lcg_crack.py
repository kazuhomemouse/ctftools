from math import gcd

if __name__ == '__main__':
    X = []
    for i in range(7):
        x = input(f'{i+1}: ')
        X.append(int(x))
    
    print('\n--------------\n')
    Y = [X[i+1] - X[i] for i in range(len(X)-1)]
    Z1 = Y[0] * Y[3] - Y[1] * Y[2]
    Z2 = Y[1] * Y[4] - Y[2] * Y[3]
    Z3 = Y[2] * Y[5] - Y[3] * Y[4]
    M = gcd(Z1, gcd(Z2, Z3))
    print(f"M: {M}")

    A = (Y[1] * pow(Y[0], -1, M)) % M
    print(f"A: {A}")

    B = (X[1] - A * X[0]) % M
    print(f"B: {B}")
 
    print('\n--------------\n')
    number = X[-1]
    for i in range(10):
        number = (A * number + B) % M
        print(f'{i}: {number}')
