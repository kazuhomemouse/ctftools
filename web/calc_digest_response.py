from hashlib import md5 

# username = "q9"
# realm="secret"
# md5(username:realm:passwd)
a1 = b"c627e19450db746b739f41b64097d449"

# uri=b"/~q9/"
uri = b"/~q9/flag.html"
req = b"GET"
a2 = md5(req + b":" + uri).hexdigest()

nonce=b"nXVj3/exBQA=7c9974c228ddfe4b38efb5581b6a9db845845299"
qop=b"auth"
nc=b"00000001"
cnonce=b"149005276508a66d"

res = md5(b':'.join([a1, nonce, nc, cnonce, qop, a2.encode('utf-8')])).hexdigest()

print(res)
